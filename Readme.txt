rigid_body_sim: A MATLAB implementation of rigid body dynamics
--------------------------------------------------------------

Readme
------

What I want to implement:
1. Unconstrained rigid body dynamics (implemented)
2. Nonpenetration constraints
3. Articulated rigid bodies

References:
http://www.cs.cmu.edu/~baraff/pbm/pbm.html