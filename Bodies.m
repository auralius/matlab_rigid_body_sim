% Class defintion for container of bodies

classdef Bodies < handle
    
    %%
    properties (Access = private)
        body = [];
        nbodies;
        
        % Hold the handles of the figure
        hndfigure = [];        
                
        % Hold the handles of the quivers, used to draw the 
        % coordinate frame   
        hndquiver;       
    end
    
    %%
    methods
        
        %%
        function obj = Bodies()
            obj.nbodies = 0;
            
            obj.hndfigure = figure;
            view([1 1 1]);            
            hold;            
        end
        
        %%
        function obj = AddBody(obj, id, body)
            body.SetId(id);
            obj.body = [obj.body body];         
            obj.nbodies = obj.nbodies + 1;
            
            % Add figure handler
            h1 = quiver3(0, 0, 0, 0, 0, 0, 'r');
            h2 = quiver3(0, 0, 0, 0, 0, 0, 'g');
            h3 = quiver3(0, 0, 0, 0, 0, 0, 'b');
            
            obj.hndquiver = [obj.hndquiver; h1 h2 h3];
        end
          
        %%
        function  InfoRigidBodies(obj)
            for i = 1 : obj.nbodies
                disp(obj.body(i));
            end
        end
        
        %%
        function obj = DrawBodies(obj)
            for i = 1 : obj.nbodies
                [x, R, P, L] = GetCurrentStates(obj.body(i));
                
                arrowx = R * [1; 0; 0] ;
                arrowx = arrowx / norm(arrowx);
                arrowy = R * [0; 1; 0];
                arrowy = arrowy / norm(arrowy);
                arrowz = R * [0; 0; 1];
                arrowz = arrowz / norm(arrowz);
                
                set(obj.hndquiver(i, 1), 'XData', x(1), 'YData', x(2), ...
                    'ZData', x(3), 'UData', arrowx(1), 'VData', arrowx(2), ...
                    'WData', arrowx(3));
                set(obj.hndquiver(i, 2), 'XData', x(1), 'YData', x(2), ...
                    'ZData', x(3), 'UData', arrowy(1), 'VData', arrowy(2), ...
                    'WData', arrowy(3));
                set(obj.hndquiver(i, 3), 'XData', x(1), 'YData', x(2), ...
                    'ZData', x(3), 'UData', arrowz(1), 'VData', arrowz(2), ...
                    'WData', arrowz(3));   
                
                drawnow;
            end
        end
        
        %%
        function obj = ApplyForce(obj, bodyindex, force)
            obj.body(bodyindex).SetForce(force);        
        end
        
        %%
        function obj = ApplyTorque(obj, bodyindex, torque)
            obj.body(bodyindex).SetTorque(torque);        
        end
        
        %% 
        function SetAxesLimit(obj, x, y, z)
            axis equal;
            xlim([x(1) x(2)]);
            ylim([y(1) y(2)]);
            zlim([z(1) z(2)]);            
            grid on;
        end
    end
    
end
