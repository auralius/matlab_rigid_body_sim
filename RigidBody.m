% Class defintion for a rigid body
% Auralius Manurung, based on: http://www.cs.cmu.edu/~baraff/pbm/pbm.html

classdef RigidBody < handle
    %%    
    properties (Access = private)
        id; 
        
        % Constant quantities
        mass;       % mass M (scalar 1x1)
        Ibody;      % body inertia (matrix 3x3)
        Ibodyinv;   % inverse of the body inertia (matrix 3x3)
        
        % State variables, need to be initialized
        x;          % the center of mass x(t) (vector 3x1)
        R;          % R(t) (matrix 3x3)
        P;          % P(t) (vector 3x1)
        L;          % L(t) (vector 3x1)
        
        % Derived quantities
        Iinv;       % Inertia in world space (3x3)
        v;          % Linear velocity (vector 3x1)
        omega;      % Rotational velocity (vector 3x1)
        
        % Computed quantities
        force;      % Vector 3x1
        torque;     % Vector 3x1   
    end
   
    %%
    methods        
        %%
        function obj = RigidBody(mass, Ibody, x0, R0)       
            
            obj.mass = mass;
            obj.Ibody = Ibody;
            obj.Ibodyinv = inv(Ibody);
            
            obj.x = x0;
            obj.R = R0;
            obj.P = [0; 0; 0];
            obj.L = [0; 0; 0];
            
            obj.Iinv = eye(3);
            obj.v = [0; 0; 0];
            obj.omega = [0; 0; 0];
            
            obj.force = [0; 0; 0];            
            obj.torque = [0; 0; 0];                        
        end        
                     
        %%
        function y = MakeY(obj)
            y(1) = obj.x(1);     % x1(t)
            y(2) = obj.x(2);     % x2(t)
            y(3) = obj.x(3);     % x3(t)
            
            y(4) = obj.R(1,1);   % R11(t)
            y(5) = obj.R(1,2);   % R12(t)
            y(6) = obj.R(1,3);   % R13(t)
            y(7) = obj.R(2,1);   % R21(t)
            y(8) = obj.R(2,2);   % R22(t)
            y(9) = obj.R(2,3);   % R23(t)
            y(10) = obj.R(3,1);  % R31(t)
            y(11) = obj.R(3,2);  % R32(t)
            y(12) = obj.R(3,3);  % R33(t)
            
            y(13) = obj.P(1);    % P1(t)
            y(14) = obj.P(2);    % P2(t)
            y(15) = obj.P(3);    % P3(t)
            
            y(16) = obj.L(1);    % L1(t)
            y(17) = obj.L(2);    % L2(t)
            y(18) = obj.L(3);    % L3(t)   
        end
        
        %%
        function y_dot = MakeYDot(obj)
            % Compute v
            obj.v = obj.P / obj.mass;
            y_dot(1) = obj.v(1);     % v1(t)
            y_dot(2) = obj.v(2);     % v2(t)
            y_dot(3) = obj.v(3);     % v3(t)
            
            % Compute R_dot
            obj.Iinv = obj.R * obj.Ibodyinv * obj.R';
            obj.omega = obj.Iinv * obj.L;
            R_dot = Star(obj.omega) * obj.R;
            
            y_dot(4) = R_dot(1,1);   % R_dot_11(t)
            y_dot(5) = R_dot(1,2);   % R_dot_12(t)
            y_dot(6) = R_dot(1,3);   % R_dot_13(t)
            y_dot(7) = R_dot(2,1);   % R_dot_21(t)
            y_dot(8) = R_dot(2,2);   % R_dot_22(t)
            y_dot(9) = R_dot(2,3);   % R_dot_23(t)
            y_dot(10) = R_dot(3,1);  % R_dot_31(t)
            y_dot(11) = R_dot(3,2);  % R_dot_32(t)
            y_dot(12) = R_dot(3,3);  % R_dot_33(t)
            
            y_dot(13) = obj.force(1); % F1(t)
            y_dot(14) = obj.force(2); % F2(t)
            y_dot(15) = obj.force(3); % F3(t)
            
            y_dot(16) = obj.torque(1); % Torque1(t)
            y_dot(17) = obj.torque(2); % Torque2(t)
            y_dot(18) = obj.torque(3); % Torque3(t)            
        end
        
        %%
        function obj = UpdateStateFromY(obj, y)
            obj.x(1) = y(1);
            obj.x(2) = y(2);
            obj.x(3) = y(3);
            
            obj.R(1,1) = y(4);
            obj.R(1,2) = y(5);
            obj.R(1,3) = y(6);
            obj.R(2,1) = y(7);
            obj.R(2,2) = y(8);
            obj.R(2,3) = y(9);
            obj.R(3,1) = y(10);
            obj.R(3,2) = y(11);
            obj.R(3,3) = y(12);
            
            obj.P(1) = y(13);
            obj.P(2) = y(14);
            obj.P(3) = y(15);
            
            obj.L(1) = y(16);
            obj.L(2) = y(17);
            obj.L(3) = y(18);                        
        end
        
        %%
        function obj = Step(obj, ts)           
            y = MakeY(obj);           
            y_dot = MakeYDot(obj);            
            y = y + y_dot .* ts;
            
            obj = UpdateStateFromY(obj, y);
        end        
        
        %%
        % Set
        function obj = SetId(obj, id)
            obj.id = id;
        end
        
        %%
        function obj = SetForce(obj, force)
            obj.force = force;
        end
        
        %%
        function obj = SetTorque(obj, torque)
            obj.torque = torque;
        end
        
        %% 
        %Get
        function [x, R, P, L] = GetCurrentStates(obj)
            x = obj.x;
            R = obj.R;
            P = obj.P;
            L = obj.L;            
        end
        
        %%
        function m = GetMass(obj)
            m = obj.mass;
        end
        
        %%
        function Ibody = GetIbody(obj)
            Ibody = obj.Ibody;
        end
        
        %%
        function v = GetVelocity(obj)
            v = obj.v;
        end
        
        %%
        function omega = GetOmega(obj)
            omega = obj.omega;
        end
     
    end    
end
