close all;
clear all;
clc;

b = Bodies;
r1 = RigidBody(1, eye(1), [0; 0; 0], rotx(0));
r2 = RigidBody(1, eye(1), [0; 2; 0], rotx(30));
r3 = RigidBody(1, eye(1), [0; 4; 0], rotx(60));

b.AddBody('b1', r1);
b.AddBody('b2', r2);
b.AddBody('b3', r3);


b.SetAxesLimit([-1 5], [-1 5], [-1 5]);

%b.ApplyForce(1, [1; 0; 0]);
b.ApplyTorque(1, [1; 0; 0]);

for i = 1 : 1000
    r1  = r1.Step(0.01);
    %pause(1);
    r1  = r1.Step(0.01);
    b = b.DrawBodies();
end



